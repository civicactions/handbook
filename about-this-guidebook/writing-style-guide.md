# Writing style

## Tone

This will be fleshed out in future iterations. For now, keep in mind that content appearing in the guidebook should be welcoming, personable, and free from jargon.

## How to capitalize titles and headings

We use sentence case for our titles and headings. When using sentence case, we capitalize the first word, all proper nouns, and a word following a colon. We do not capitalize the next word if a title or heading begins with a number.

Examples

- Site building using Drupal
- Engineer's role in client relationships
- Accessibility: Everyone has a role

## Suggestions

The [GitHub Actions build](github-actions.md) outputs a list of suggestions to improve the readability, language, and grammar of the file(s) your pull requests touches. You are encouraged to review and incorporate these.

## Specific terms

- Never refer to "Open Source" Software without specifying it Free/Libre Open Source Software, which can be shortened to FLOSS. See [Richard Stallman's explanation](https://www.gnu.org/philosophy/floss-and-foss.en.html) if you want to know more.
