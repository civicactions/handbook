# When your project has a contractual Modification (MOD) or Extension

## What is the difference between a contract MOD and an Extension?

Contract Modifications (MOD) are required for any change to the original document, e.g. adjustments to the following:

- key personnel
- scope
- deliverables
- contract amount

Contract Extensions generally involve retaining the contractual language yet adjusting the given dates (e.g., for option terms or option years).

## Contractual changes

Any contractual changes require input from the CivicActions Legal team.

## MODs and Extensions are a good time to re-evaluate

If your project requires a MOD or Extension, it is prudent to ensure everyone agrees about <i>why</i> the change is being made. Pull in your Project Management colleagues for input, as well as CivicActions leaders, i.e., Sales.

## Common steps for a MOD or Extension

- Start with a short summary document which outlines the following:
  - Why the change is required
  - The current contractual language
  - The proposed contractual language (e.g., from old paragraphs, page numbers, etc. to replacement language matched to old paragraphs, page numbers, etc.)
- Schedule an internal call to review
- Work with Legal and Sales to confirm language and strategy
