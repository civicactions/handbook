# Project Management Checklists

The Project Management practice area suggests leveraging the following checklists for all projects:

## Monthly Checklists (to be completed by the third business day of each month)

Wrap up the past month:

1. Complete monthly contractual deliverables (monthly reporting, etc.)
2. Run the "plan vs actual" report for the last month and in order to verify reporting accuracy

Now prepare for the new month:

3. Remind your team about our Out of Office (OOO) procedure
4. Update Unanet with Plans (subtracting any OOO or company holidays) for all project resources
5. Assign Plans
6. Update any project-related scheduling (if needed)
7. Review your meeting cadence: Can any meetings be dropped to optimize focus yet while maintaining strategy?
   Note: It sometimes help to ask, "What kinds of questions should we be asking ourselves about meetings and if they are delivering value?"
8. Clean up project folders

## Quarterly Checklists (to be completed by the fifth business day of each quarter)

Wrap up the past quarter:

1. Adjust onboarding decks with any new project resources
2. Adjust the All Projects deck with any changes
3. Adjust the Slack index

Now prepare for the new quarter:

3. Review project Wrappers (Legal and Project Management)

   - Review contractual dates and details
   - Review QASP
   - Confirm deliverables are being met
   - Verify alignment with DevOps around deliverables
   - Confirm scope has not changed

4. Review Team Working Agreements
