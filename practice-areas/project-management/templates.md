# Project Manager Templates

## Purpose

Templates are a great way to become efficient at communicating project status. They also allow us, as a practice area, to deliver consistency across projects. The templates below are commonly used by Project Managers throughout the life of a project. All of the templates are stored within the [CivicActions Project Management Folder](https://drive.google.com/drive/folders/0B8h3s4uUHTrZX0g0d1gzWVJ2OHM). Given the ongoing improvements of our team, this page is dynamic.

## Budget Tracking

The following templates can be helpful for budget tracking to include in weekly status reporting.

[Burndown Template](https://docs.google.com/spreadsheets/d/16fvmQ6Rfg0YeTuSFWU_j81a73PMQtPd8kBONyjVZCDw/edit#gid=0)

[Budget Tracking Document](https://docs.google.com/spreadsheets/d/1fCFzC_7dLe6diXWK8_yzW91svJFGnYbwEMLAPI-tDeU/edit#gid=1956976072)

## Client Change Request

Some project contracts require formal change requests when modifying key personnel, deliverables, budget, etc. This document can serve for initiating change with the contracting officer representative (COR).

[Client Change Request Template](https://docs.google.com/a/civicactions.net/document/d/1575TtYmZEu0vpsKlAXwXTFYc1HPhb0L5_ACiG1DL1jU/edit?usp=sharing)

## Client Integrated Project Plan

If the project requires an Integrated Project Plan (IPP), this template is a good starting point. This is not a large fixed plan, instead a living document as we learn more about the project. It will be important that the Project Manager updates this plan as more is discovered.

[Client Integrated Project Plan Template](https://docs.google.com/a/civicactions.net/document/d/1cqh2-QpfvZYQDt9IHbA-vEqY9hrjoiJ1Nb-xdbQ6loU/edit?usp=sharing)

## Decision Log

Decision tracking supports project management by documenting all major pivot points.

[Decision Log](https://docs.google.com/spreadsheets/d/1d2ye6y7A7O0boKpgzZdvkNffUp5LEP0CXTWCh1dNMD0/edit#gid=0)

## Kickoff Agenda

This template is used as a guide for project managers when beginning a project. The agenda provides a list of key points the Project Manager can address during to ensure there are clear expectations of all participating parties.

[Kickoff Meeting Agenda Template](https://docs.google.com/a/civicactions.net/document/d/1pmOruj_1PeSfmJtxzvjDy7KxTTJi0VS8D62WUrWjeSM/edit?usp=sharing)

## Kickoff Deck

A newer template, this deck serves as a starting place for the key topics to include on any Kickoff call.

[Kickoff Deck](https://docs.google.com/presentation/d/18ZN9EP34qLrsWjE7WhsfyRH54DTV3jOA3rRWzXY7bKM/edit)

## Meeting Notes

Used as a guide for Project Managers when facilitating daily scrum calls; the team can decide the desired format.

[Meeting Notes template](https://docs.google.com/a/civicactions.net/document/d/17tl3lPu-3Uo6_YCEtb6AH9HsaILLS1UTmoUFIuXoqDc/edit?usp=sharing)

## OOO Coverage

A few templates that support Project Managers for OOO planning and returning.

[Coverage Plan](https://docs.google.com/document/d/1NRTpzfLi3onlV6IybPhNWtX1C24vqNAYNL51QFcStbA/edit#heading=h.3uhyu8z8uyfi)

[Returning Project Manager/ Handoff](https://docs.google.com/document/d/1gipi6rkYyJdPDkbfcfqSGSPHcy4EnngsqaGiqd-EAfA/edit)

## Privledged Users

To maintain the integrity of all Production systems, this template is an efficiant way to track access rights for quarterly reviews.

[Project Privledged Users](https://docs.google.com/spreadsheets/d/1RJDY64ZLhUYnX3kM7QrfEJaCUW_VLflXLJ9VZgGn6ZI/edit#gid=0)

## Project Brief (aka Onboarding Deck)

All projects intend to have an Onboarding deck for both CivicActions and partners, with the purpose of provide an overarching view of the projecs.

[Project Brief](https://docs.google.com/presentation/d/1Vk7DKxe5Sop6T3JhudOcLGCffH8XJXDiX8WtBbH3Te0/edit#slide=id.gd85224ffcd_0_0)

## Project Contacts

This template is useful for projects that have a large stakeholder team or when the project team has a partner team.

[Project Contacts Template](https://docs.google.com/spreadsheets/d/14dgvmMI4cmIGzLOldfRaPHh65j4BKu03v9hRWxEtA1A/edit?usp=drive_web&ouid=103893616702532363241)

## Project and Product Management Hacks for Designers

A great resource for opening project and product management duties to the full team, and especially for our design colleagues.

[Project and Product Management Hacks for Designers](https://docs.google.com/presentation/d/1iNm4p0KZjxst5S35DRQ1v7UyNM2dz0p6OVtDGowuA80/edit#slide=id.g11625fcf513_0_96)

## Risk Ledger

This template is used as a guide for Project Managers to track all project risks.

[Risk Ledger Template](https://docs.google.com/spreadsheets/d/1sOfCcARTNAaP0PfyMY9iBiThONGbzKZrzn4adrmpVAw/edit#gid=0)

## Team Working Agreement

A new template, still being polished, that allows for teams to unite around their core tenets. See (link) for instructions for Project Managers.

[Team Working Agreement](https://docs.google.com/document/d/1Wkx-o0Me0dqbYv-dqt6IQcvoq5M-3v--HNQPSfzDe1M/edit#heading=h.2fgd0t58pes3)

[Elected Priorities](https://docs.google.com/forms/d/1f1hnFe-ZvjEU-MXOSJAB3UyOuedwMp_ZsRjpKRIUrxA/edit)

## Weekly Status Report

This template is used as a guide for Project Managers to send weekly status updates to their Product Owner / client. We want our clients to have access to this type of important reporting easily and often. This status report provides the client with critical budget updates, lists new risks, outlines any major decisions, provides a summary of the work that week, etc. If a project calls for a monthly status report (MSR), these weekly reports should provide all the information to construct the MSR.

[Weekly Status Report Template](https://docs.google.com/a/civicactions.net/document/d/1ZFOjwys-jz8WCrqzBatCYB5_4e9v3W1kBR5yFo8NJgc/edit?usp=sharing)

## Wrapper/ Legal

A template required for each project and managed by our Legal team. This is updated quarterly to confirm alignment with all current contract details.

Once completed, the Legal Wrapper is owned by our Legal team, retained in their folders, and is read-only. It is transparent, however, for all of CivicActions.

[Legal Wrapper](https://docs.google.com/document/d/1BTkjJ2N8iPPsuxjb3cLfaqe30nfVQHSgs8cuWaFKalw/edit)

## Wrapper/ Project Management

The companion to the Legal Wrapper, with all details needed for the Project Manager. This is also updated quarterly to confirm all current contact details are accurate.

Once completed, the Project Management Wrapper is owned by our practice area, retained in the project Internal folder/ Wrapper. It is transparent for all of CivicActions.

[Project Management Wrapper](https://docs.google.com/document/d/1eh6L6IiBF6DbOKzIPM2pRZXhBFBb0hbVVtq-f8HlxEQ/edit#heading=h.2fgd0t58pes3)
